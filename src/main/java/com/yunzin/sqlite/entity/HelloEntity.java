package com.yunzin.sqlite.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

@Data
@TableName("hello")
public class HelloEntity implements Serializable {

    @TableId
    private Long id;

    private String title;

    private String text;
}

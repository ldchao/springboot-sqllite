package com.yunzin.sqlite.controller;

import com.yunzin.sqlite.entity.HelloEntity;
import com.yunzin.sqlite.service.HelloService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/hello")
public class HelloWorld {

    @Autowired
    private HelloService helloService;

    @RequestMapping("/list")
    public List<HelloEntity> list(@RequestParam Map<String, Object> params) {
        List<HelloEntity> list = helloService.list();

        return list;
    }

    @RequestMapping("/info/{id}")
    public HelloEntity info(@PathVariable("id") Long id) {
        HelloEntity entity = helloService.getById(id);

        return entity;
    }

    @RequestMapping("/save")
    public Integer save(@RequestBody HelloEntity entity) {

        helloService.save(entity);

        return 1;
    }

    @RequestMapping("/update")
    public Integer update(@RequestBody HelloEntity entity) {

        helloService.updateById(entity);

        return 1;
    }
}

package com.yunzin.sqlite.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yunzin.sqlite.entity.HelloEntity;
import com.yunzin.sqlite.dao.HelloDao;
import com.yunzin.sqlite.service.HelloService;
import org.springframework.stereotype.Service;

@Service("helloService")
public class HelloServiceImpl extends ServiceImpl<HelloDao, HelloEntity> implements HelloService {
}
